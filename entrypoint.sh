#!/bin/bash
echo "Running migrations"
python manage.py migrate
echo "Running migrations"
python manage.py collectstatic --no-input

# Prepare log files and start outputting logs to stdout
echo "touch /srv/logs/gunicorn.log"
touch /srv/logs/gunicorn.log
echo "touch /srv/logs/access.log"
touch /srv/logs/access.log
echo "tail -n 0 -f /srv/logs/*.log &"
tail -n 0 -f /srv/logs/*.log &

# Start Gunicorn processes
echo Starting Gunicorn.
exec gunicorn backend.wsgi:application \
    --name backend \
    --bind 0.0.0.0:8000 \
    --workers 3 \
    # --log-level=info \
    # --log-file=/srv/logs/gunicorn.log \
    # --access-logfile=/srv/logs/access.log \
    "$@"