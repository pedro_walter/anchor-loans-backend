from rest_framework.permissions import BasePermission


class UserIsApprover(BasePermission):
    def has_permission(self, request, view):
        user = request.user
        return (not user.is_anonymous
                and user.groups.filter(name='Approver').exists())
