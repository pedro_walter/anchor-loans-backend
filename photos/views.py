from django_filters import rest_framework as filters
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .models import Comment, Like, Photo
from .permissions import UserIsApprover
from .serializers import (CommentSerializer,
                          PhotoSerializer,
                          PhotoSerializerCreate)


class PhotoFilter(filters.FilterSet):
    status = filters.NumberFilter()


class PhotoViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing accounts.
    """
    queryset = Photo.objects.all()
    lookup_field = 'uuid'
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = PhotoFilter

    def get_permissions(self):
        if self.action in ['approve', 'deny']:
            self.permission_classes = (UserIsApprover,)
        elif self.action in ['list', 'retrieve']:
            self.permission_classes = (AllowAny,)
        else:
            self.permission_classes = (IsAuthenticated,)
        return super(PhotoViewSet, self).get_permissions()

    def get_serializer_class(self):
        if self.action == 'create':
            return PhotoSerializerCreate
        return PhotoSerializer

    @action(
        detail=True,
        methods=['POST']
    )
    def approve(self, request, uuid):
        photo = self.get_object()
        photo.status = Photo.APPROVED
        photo.save()
        serializer = self.get_serializer(photo)
        return Response(serializer.data)

    @action(
        detail=True,
        methods=['POST']
    )
    def deny(self, request, uuid):
        photo = self.get_object()
        photo.status = Photo.DENIED
        photo.save()
        serializer = self.get_serializer(photo)
        return Response(serializer.data)

    @action(
        detail=True,
        methods=['POST']
    )
    def like(self, request, uuid):
        photo = self.get_object()
        user = request.user
        Like.objects.create(photo=photo, creator=user)
        return Response({'details': 'liked succesfully'})

    @action(
        detail=True,
        methods=['POST']
    )
    def unlike(self, request, uuid):
        photo = self.get_object()
        user = request.user
        Like.objects.filter(photo=photo, creator=user).delete()
        return Response({'details': 'disliked succesfully'})

    @action(
        detail=True,
        methods=['POST'],
        url_path="add-comment"
    )
    def add_comment(self, request, uuid):
        photo = self.get_object()
        user = request.user
        comment = Comment.objects.create(photo=photo,
                                         creator=user,
                                         text=request.data.get('text'))
        serializer = CommentSerializer(comment)
        return Response(serializer.data, status=201)
