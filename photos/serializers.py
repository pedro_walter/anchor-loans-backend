from rest_framework import serializers

from .models import Comment, Like, Photo


class CommentSerializer(serializers.ModelSerializer):
    creator = serializers.CharField(source='creator.username')

    class Meta:
        model = Comment
        exclude = ['id', 'photo']


class PhotoSerializer(serializers.ModelSerializer):
    status = serializers.CharField(source='get_status_display')
    liked = serializers.SerializerMethodField()
    likes = serializers.IntegerField()
    comments = CommentSerializer(many=True, source="comment_set.all")

    def get_liked(self, obj):
        if self.context['request'].user.is_anonymous:
            return False
        return Like.objects.filter(
            photo=obj, creator=self.context['request'].user
        ).exists()

    class Meta:
        model = Photo
        exclude = ['id']


class PhotoSerializerCreate(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = ['file', 'title']

    def create(self, validated_data):
        validated_data['creator'] = self.context['request'].user
        photo = Photo.objects.create(**validated_data)
        return photo
