from django.contrib import admin

from .models import Like, Photo

# Register your models here.
admin.site.register(Like)
admin.site.register(Photo)
