import uuid

from django.contrib.auth.models import User
from django.db import models


class HasUUID(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    class Meta:
        abstract = True


class Created(models.Model):
    created = models.DateTimeField(editable=False, auto_now_add=True)

    class Meta:
        abstract = True


class Updated(models.Model):
    updated = models.DateTimeField(editable=False, auto_now=True)

    class Meta:
        abstract = True


class Creator(models.Model):
    creator = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    class Meta:
        abstract = True


class InvalidTransitionException(Exception):
    pass


class Photo(HasUUID, Created, Updated, Creator):
    PENDING_APPROVAL = 1
    APPROVED = 2
    DENIED = 3
    STATUS_CHOICES = (
        (PENDING_APPROVAL, "PENDING_APPROVAL"),
        (APPROVED, "APPROVED"),
        (DENIED, "DENIED"),
    )

    file = models.FileField()
    title = models.CharField(max_length=255)
    status = models.IntegerField(choices=STATUS_CHOICES,
                                 default=PENDING_APPROVAL)

    @property
    def likes(self):
        return self.like_set.count()

    def approve(self):
        if self.status == self.PENDING_APPROVAL:
            self.status = self.APPROVED
            self.save()
        else:
            raise InvalidTransitionException(
                "Can only approve photos pending approval")

    def deny(self):
        self.status = self.DENIED
        self.save()

    def __str__(self):
        return self.title


class Like(Created, Updated, Creator):
    photo = models.ForeignKey(Photo, on_delete=models.CASCADE)

    def __str__(self):
        return "Like by user {} on photo {}".format(self.user.id,
                                                    self.photo.uuid)

    class Meta:
        unique_together = ('creator', 'photo')


class Comment(HasUUID, Created, Updated, Creator):
    photo = models.ForeignKey(Photo, on_delete=models.CASCADE)
    text = models.TextField()

    def __str__(self):
        return "Comment by user {} on photo {}".format(self.creator.username,
                                                       self.photo.uuid)
