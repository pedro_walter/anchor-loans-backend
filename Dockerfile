FROM python:3.6-buster

ADD ./backend /code/backend
ADD ./photos /code/photos
ADD ./manage.py /code
ADD ./requirements.txt /code

WORKDIR /code

RUN apt-get update
# RUN apt-cache search python3
# RUN apt-cache search build
RUN apt-get install -y build-essential libssl-dev libffi-dev python3-dev cargo

RUN pip install -r requirements.txt

EXPOSE 8000

COPY ./entrypoint.sh /
# # RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]