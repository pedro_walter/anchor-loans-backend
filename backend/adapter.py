from urllib.parse import urljoin
import os

from allauth.account.adapter import DefaultAccountAdapter


class OurAccountAdapter(DefaultAccountAdapter):
    def get_email_confirmation_url(self, request, emailconfirmation):
        server_url = os.environ.get('SERVER_URL')
        if server_url:
            return urljoin(server_url,
                           f"/validate-account/{emailconfirmation.key}/")
        return super().get_email_confirmation_url(request, emailconfirmation)
