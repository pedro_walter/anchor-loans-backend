from rest_framework import serializers
from rest_auth import serializers as auth_serializers
from django.contrib.auth.models import User


class UserDetailsSerializer(auth_serializers.UserDetailsSerializer):
    is_approver = serializers.SerializerMethodField()

    def get_is_approver(self, obj):
        return obj.groups.filter(name='Approver').exists()

    class Meta:
        model = User
        fields = ['id', 'is_approver']
